let got = require('./data-1.cjs')
// console.log(got);
function countAllPeople() {
  // your code goes here
  const houses = got.houses;
  
  let countAll = houses.reduce((acc,element)=>{
    return acc+=element.people.length;
       
},0);
return countAll;


}

function peopleByHouses() {
  // your code goes here
  const houses = got.houses;
  const obj = {}
  houses.forEach((element)=>{
       obj[element.name]=element.people.length;
});
const sortedObj= Object.fromEntries(Object.entries(obj).sort());


return sortedObj;
}

function everyone() {
  // your code goes here
  const houses = got.houses;
  const peoples = []
  houses.forEach((element)=>{
       const people = element.people;
       people.forEach((ele)=>{
        peoples.push(ele.name)
       })
});
return peoples;
}

function nameWithS() {
  // your code goes here
  const houses = got.houses;
  // const peoples = []
//   houses.forEach((element)=>{
//        const people = element.people;
//        people.forEach((ele)=>{
//         const name = ele.name;
//         if(name.includes('s') || name.includes('S')){
//         peoples.push(name)
//       }
//        })
// });
let peoples = []
houses.forEach((element) =>{
  let filterArray=element.people.filter((person)=>
  person.name.includes('s') || person.name.includes('S')).map((person)=>person.name);
  peoples = peoples.concat(filterArray);
});
 


return peoples;
}

function nameWithA() {
  // your code goes here  
   const houses = got.houses;
//   const peoples = []
//   houses.forEach((element)=>{
//        const people = element.people;
//        people.forEach((ele)=>{
//         const name = ele.name;
//         if(name.includes('a') || name.includes('A')){
//         peoples.push(name)
//       }
//        })
// });
let peoples = []
houses.forEach((element) =>{
  let filterArray=element.people.filter((person)=>
  person.name.includes('a') || person.name.includes('A')).map((person)=>person.name);
  peoples = peoples.concat(filterArray);
});
 


return peoples;

}

function surnameWithS() {
  // your code goes here
  const houses = got.houses;
  const peoples = []
  houses.forEach((element)=>{
       const people = element.people;
       people.forEach((ele)=>{
        const surname = ele.name.split(' ')[1];
        if(surname.startsWith('s') || surname.startsWith('S')){
        peoples.push(ele.name)
      }
       })
});
return peoples;
}

function surnameWithA() {
  // your code goes here
  const houses = got.houses;
  let peoples = []
//   houses.forEach((element)=>{
//        const people = element.people;
//        people.forEach((ele)=>{
//         const surname = ele.name.split(' ')[1];
//         if(surname.startsWith('a') || surname.startsWith('A')){
//         peoples.push(ele.name)
//       }
//        })
// });
houses.forEach((element)=>{
  let filterArr = element.people.filter((person)=>{
    const surname = person.name.split(' ')[1];
    return surname.startsWith('a') || surname.startsWith('A')
  }).map((person)=>person.name)
  peoples = peoples.concat(filterArr);
})
return peoples;
}

function peopleNameOfAllHouses() {
  // your code goes here
  const houses = got.houses;
  const peoplesByHouse = {}

  houses.forEach((element)=>{
         const house =element.name;
        peopleByHouses[house]=[]
       const people = element.people;
       people.forEach((ele)=>{
        peopleByHouses[house].push(ele.name);
      
       })
});
const sortedObj= Object.fromEntries(Object.entries(peopleByHouses).sort());
return sortedObj;
}

// Testing your result after writing your function
console.log(countAllPeople());
// Output should be 33

console.log(peopleByHouses());
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone());
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(), 'with s');
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA());
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(), 'surname with s');
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA());
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses());
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}
